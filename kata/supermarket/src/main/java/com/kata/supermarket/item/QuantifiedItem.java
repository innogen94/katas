package com.kata.supermarket.item;

import com.kata.supermarket.exception.InvalidQuantifiedItemException;
import com.kata.supermarket.pricing.Price;
import com.kata.supermarket.pricing.strategy.PricingStrategy;
import com.kata.supermarket.utils.CommonUtil;

import lombok.Data;

/**
 * Describes quantified item entity
 * 
 * @author Volodia WOJCIUK
 */
@Data
public class QuantifiedItem implements Discountable {

    /**
     * Quantified item properties
     */
    private Item item;
    private Quantity quantity;

    /**
     * Constructs a quantified item
     * 
     * @param item     the given item
     * @param quantity the item quantity
     */
    private QuantifiedItem(Item item, Quantity quantity) {

        if (CommonUtil.isNull(item)) {
            throw new InvalidQuantifiedItemException(
                "One or several parameters are invalid");
        }
        this.setItem(item);
        this.setQuantity(quantity);
    }

    /**
     * Creates a new quantified item
     * 
     * @param item     the item to be quantifed
     * @param quantity the item quantity
     * @return the new quantified item
     */
    public static QuantifiedItem create(Item item, Quantity quantity) {

        return (new QuantifiedItem(item, quantity));
    }

    /**
     * Increases the quantity by one unit
     */
    public void increaseQuantity() {

        this.quantity.increase();
    }

    /**
     * Increases the quantity by a given value
     * 
     * @param value the value to add
     */
    public void increaseQuantityBy(int value) {

        this.quantity.increaseBy(value);
    }

    /**
     * Decreases the quantity by one unit
     */
    public void decreaseQuantity() {

        this.decreaseQuantityBy(Quantity.of(1));
    }

    /**
     * Decreases the quantity by a given value
     * 
     * @param quantity the quantity to add
     */
    public void decreaseQuantityBy(Quantity quantity) {

        this.setQuantity(this.quantity.substractBy(quantity));
    }

    /**
     * Reset the item quantity to zero
     */
    public void resetQuantity() {

        this.resetQuantityTo(Quantity.of(0));
    }

    /**
     * Reset the item quantity to a given value
     * 
     * @param quantity the new quantity
     */
    public void resetQuantityTo(Quantity quantity) {

        this.quantity = quantity;
    }

    /**
     * Calculates the total price
     * 
     * @return the price
     */
    public Price getTotalPrice() {

        return (this.item.getPrice().multiplyBy(this.quantity));
    }

    /**
     * Applies discount strategy to the current quantified item
     */
    @Override
    public Price discountWith(PricingStrategy pricingStrategy) {
        
        return (pricingStrategy.applyTo(this));
    }
}