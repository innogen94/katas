package com.kata.supermarket.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.kata.supermarket.exception.ItemNotFoundException;
import com.kata.supermarket.exception.NotEnoughItemQuantityException;
import com.kata.supermarket.item.*;

import org.springframework.stereotype.Component;

@Component
public class ItemRepository {
 
    /**
     * Item repository properties
     */
    private Map<String, QuantifiedItem> itemStore = new HashMap<>();

    /**
     * Create a new item repository
     * 
     * @return the item repository
     */
    public static ItemRepository create() {

        return (new ItemRepository());
    }

    /**
     * Add a new item to the store
     * 
     * @param item the item to be stored
     */
    public void addItem(Item item) {

        this.addItem(item, Quantity.of(1));
    }

    /**
     * Add a new item to the store with a given quantity
     * 
     * @param item the item to be stored
     * @param quantity the quantity of the item
     */
    public void addItem(Item item, Quantity quantity) {

        this.itemStore.put(
            item.getName(), QuantifiedItem.create(item, quantity));
    }

    /**
     * Get item according to the item name
     * 
     * @param name the item name
     */
    public Item getItem(String name) {

        if (!this.itemStore.containsKey(name)) {
            throw new ItemNotFoundException(String.format(
                "None item has been found with the name <%s>\n", name));
        }
        QuantifiedItem quantiItem = this.itemStore.get(name);
        Item item = quantiItem.getItem();

        return (item);
    }

    /**
     * Checks if an item is registered under a given name
     * 
     * @param itemName the name of the item
     * @return the boolean status
     */
    public boolean isItemRegistered(String itemName) {

        return (this.itemStore.containsKey(itemName));
    }

    /**
     * Get quantity for a given item
     * 
     * @param itemName the name of the item
     * @return the item quantity
     * 
     * @throws ItemNotFoundException
     */
    public Quantity getQuantity(String itemName) {

        if (!this.itemStore.containsKey(itemName)) {
            throw new ItemNotFoundException(String.format(
                "There is none item registered with the name <%s>\n", itemName));
        }
        QuantifiedItem quantifiedItem = this.itemStore.get(itemName);
        Quantity itemQuantity = quantifiedItem.getQuantity();

        return (itemQuantity);
    }

    /**
     * Get quantified item with its name and a desired quantity
     * 
     * @param itemName the target item's name
     * @param quantity the item desired quantity
     * @return the quantified item
     * 
     * @throws ItemNotFoundException
     */
    public QuantifiedItem getItems(String itemName, Quantity quantity) {

        if (!this.itemStore.containsKey(itemName)) {
            throw new ItemNotFoundException(String.format(
                "There is none item registered with the name <%s>\n", itemName));
        }
        QuantifiedItem quantifiedItem = this.itemStore.get(itemName);
        Item item = quantifiedItem.getItem();

        try {
            quantifiedItem.decreaseQuantityBy(quantity);
        } catch (NotEnoughItemQuantityException ex) {

            QuantifiedItem limitedQuantifiedItem = 
                QuantifiedItem.create(item, quantifiedItem.getQuantity());
            quantifiedItem.resetQuantity();
            return (limitedQuantifiedItem);    
        }
        return (QuantifiedItem.create(item, quantity));
    }

    /**
     * Recovers all quantified items
     * 
     * @return the quantified items
     */
    public List<QuantifiedItem> getAllItems() {

        List<QuantifiedItem> allQuantifiedItems = 
            this.itemStore.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());

		return (allQuantifiedItems);
    }

    /**
     * Removes all items
     */
    public void flush() {

        this.itemStore.clear();
    }
}