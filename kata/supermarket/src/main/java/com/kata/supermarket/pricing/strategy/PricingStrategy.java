package com.kata.supermarket.pricing.strategy;

import com.kata.supermarket.item.*;
import com.kata.supermarket.pricing.*;

import lombok.Data;

@Data
public abstract class PricingStrategy {
    
    /**
     * Properties
     */
    private String name;

    /**
     * Initializes princing strategy
     * 
     * @param name the name of the strategy
     */
    public PricingStrategy(String name) {

        this.setName(name);
    }

    /**
     * Applies a given pricing strategy for a given quantified item
     * 
     * @param quantifiedItem the quantified item
     * @return 
     */
    public abstract Price applyTo(QuantifiedItem quantifiedItem);
    
}