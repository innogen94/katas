package com.kata.supermarket.exception;

/**
 * Describes an Invalid Item Exception
 * 
 * @author Volodia WOJCIUK
 */
public class ItemNotFoundException  extends RuntimeException {
    
    /**
     * The 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new Item Not Found Exception
     * 
     * @param message the message of the exception
     */
    public ItemNotFoundException(String message) {

        super(String.format("Item Not Found Exception. Cause: %s\n", message));
    }
}