package com.kata.supermarket.exception;

public class NotEnoughItemQuantityException extends RuntimeException {
    
    /**
     * Serial number
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs the exception
     * 
     * @param message the exception message
     */
    public NotEnoughItemQuantityException(String message) {

        super(String.format("NotEnoughItemQuantityException. Cause: %s\n", message));
    }
}