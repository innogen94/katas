package com.kata.supermarket.exception;

public class InvalidPriceException extends RuntimeException {

    /**
     * Serial number
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Initializes the exception
     * 
     * @param message the exception message
     */
    public InvalidPriceException(String message) {

        super(message);
    }

}