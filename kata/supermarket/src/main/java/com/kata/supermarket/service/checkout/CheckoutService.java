package com.kata.supermarket.service.checkout;

import java.util.List;
import java.util.Optional;

import com.kata.supermarket.item.Item;
import com.kata.supermarket.item.QuantifiedItem;
import com.kata.supermarket.pricing.Price;
import com.kata.supermarket.pricing.strategy.PricingStrategy;
import com.kata.supermarket.repository.StrategyRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Describes a checkout scanning actions
 * 
 * @author Volodia WOJCIUK
 */
@Service
public class CheckoutService {
    
    /**
     * The strategy repository
     */
    private final StrategyRepository strategyRepository;

    /**
     * Initialize Checkout component
     * 
     * @param strategyRepository the strategy repository
     */
    @Autowired
    public CheckoutService(StrategyRepository strategyRepository) {

        this.strategyRepository = strategyRepository;
    }

    /**
     * Scans a quantified item and calculates 
     * its price according to pricing rules
     * 
     * @param quantifiedItem the quantified item to scan
     * @return the total price with discount
     */
    public Price scan(QuantifiedItem quantifiedItem) {

        Item item = quantifiedItem.getItem();
        Optional<PricingStrategy> pricingStrategy = 
            this.strategyRepository.getPricingStrategy(item);

        if (pricingStrategy.isPresent()) {
            
            Price discountedPrice = 
                quantifiedItem.discountWith(pricingStrategy.get());
            return (discountedPrice);
        }
        return (quantifiedItem.getTotalPrice());
    }

    /**
     * Scans a list of quantified item and calculates 
     * the total price of all items according to pricing rules
     * 
     * @param quantifiedItems the quantified items list
     * @return the total price of all items
     */
    public Price scan(List<QuantifiedItem> quantifiedItems) {

        Price totalPrice = quantifiedItems
            .stream()
                .map(quantifiedItem -> this.scan(quantifiedItem))
                .map(price -> price.getValue())
                    .reduce((current, next) -> current + next)
                .map(reducedPrice -> Price.of(reducedPrice))
			.get();

        return (totalPrice);
    }
    
}