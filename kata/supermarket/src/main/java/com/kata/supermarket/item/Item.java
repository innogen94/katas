package com.kata.supermarket.item;

import java.util.Optional;

import com.kata.supermarket.exception.InvalidItemException;
import com.kata.supermarket.pricing.*;

import lombok.Data;

/**
 * Describes supermarket item entity
 * 
 * @author Volodia WOJCIUK
 */
@Data
public class Item {
    
    /**
     * Item properties
     */
    private String name;
    private Optional<String> description;
    private Price price;

    /**
     * Constructs a new item with 
     * its name and price without description
     * 
     * @param name the name of the item
     * @param price the price of the item
     */
    private Item(String name, Price price) {

        if ((name == null || name.isEmpty()) || price == null) {
            throw new InvalidItemException(
                "One or several parameters are invalid");
        }
        this.setName(name);
        this.setPrice(price);
    }

    /**
     * Constructs a new item 
     * with its name, description and price
     * 
     * @param name the name of the item
     * @param description the description of the item
     * @param price the price of the item
     */
    private Item(String name, Price price, String description) {

        if ((name == null || name.isEmpty()) || price == null) {
            throw new InvalidItemException(
                "One or several parameters are invalid");
        }
        this.setName(name);
        this.setDescription(
            Optional.of(description));
        this.setPrice(price);
    }

    /**
     * Create a new item with 
     * its name and price without description
     * 
     * @param name the name of the item
     * @param price the price of the item
     * @return the new item
     */
    public static Item create(String name, Price price) {
        
        return (new Item(name, price));
    }

    /**
     * Create a new item with its name, price and description
     * 
     * @param name the name of the item
     * @param price the price of the item
     * @return the new item
     */
    public static Item create(String name, Price price, String description) {
        
        return (new Item(name, price, description));
    }

}