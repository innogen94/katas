package com.kata.supermarket.exception;

/**
 * Exception thrown when not pricing strategy has not been 
 * found for a given item in the strategy repository
 * 
 * @author Volodia WOJCIUK
 */
public class PricingStrategyNotFoundException extends RuntimeException {
    
    /**
     * Serial number
     */
    private static final long serialVersionUID = 1L;

    /**
     * Initialization
     * 
     * @param message the exception message
     */
    public PricingStrategyNotFoundException(String message) {

        super(message);
    }
}