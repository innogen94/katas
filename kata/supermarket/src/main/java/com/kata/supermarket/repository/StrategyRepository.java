package com.kata.supermarket.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.kata.supermarket.item.Item;
import com.kata.supermarket.pricing.strategy.PricingStrategy;

import org.springframework.stereotype.Component;

/**
 * StrategyRepository
 */
@Component
public class StrategyRepository {

    /**
     * Properties
     */
    private Map<String, PricingStrategy> pricingStrategyByItemNameMap = new HashMap<>();
    private Map<Integer, PricingStrategy> allPricingStrategies = new HashMap<>();

    /**
     * Create a new strategy repository
     * 
     * @return the strategy repository
     */
    public static StrategyRepository create() {

        return (new StrategyRepository());
    }

    /**
     * Add a new pricing strategy to repository
     * 
     * @param strategy the pricing strategy
     * @param item the given item
     */
    public void addPricingStrategyToItem(PricingStrategy strategy, Item item) {

        if (!Optional.of(strategy).isPresent() || !Optional.of(item).isPresent()) {
            throw new NullPointerException(
                "One or several parameters are invalid.");
        }
        this.pricingStrategyByItemNameMap.put(item.getName(), strategy);
        this.allPricingStrategies.put(strategy.hashCode(), strategy);
    }

    /**
     * Finds pricing strategy for a given item
     * 
     * @param item the target item
     * @return the corresponding pricing strategy
     */
    public Optional<PricingStrategy> getPricingStrategy(Item item) {

        Optional<PricingStrategy> pricingStrategy = Optional.ofNullable(
            this.pricingStrategyByItemNameMap.get(item.getName()));
        
        return (pricingStrategy);
    }
}