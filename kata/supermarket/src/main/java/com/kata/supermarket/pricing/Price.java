package com.kata.supermarket.pricing;

import com.kata.supermarket.exception.InvalidPriceException;
import com.kata.supermarket.item.Quantity;

import lombok.Data;

/**
 * Describes the price entity
 * 
 * @author Volodia WOJCIUK
 */
@Data
public class Price/* implements Comparable*/ {

    /**
     * Price poperties
     */
    private float value;

    /**
     * All args contructor
     * 
     * @param value
     */
    private Price(float value) {

        if (Float.compare(value, 0.0f) < 0) {
            throw new InvalidPriceException("A price cannot be negative");
        }
        this.setValue(value);
    }

    /**
     * Create a new price
     * 
     * @param value the value of the price
     * @return the new price
     */
    public static Price of(float value) {

        return (new Price(value));
    }

    /**
     * Check if the value of the price is equal to zero
     * 
     * @return the boolean status
     */
    public boolean isEqualToZero() {

        return (Float.compare(this.value, 0.0f) == 0);
    }

    /**
     * Check if the value of the price is negative
     * 
     * @return the boolean status
     */
    public boolean isPositive() {

        return (Float.compare(this.value, 0.0f) > 0);
    }

    /**
     * Multiplies the current price by a scalar value
     * 
     * @param quantity the quantity
     * @return the new price
     */
    public Price multiplyBy(Quantity quantity) {

        return (new Price(this.value * quantity.getValue()));
    }

    /**
     * Multiplies the current price by a scalar value
     * 
     * @param value the value
     * @return the new price
     */
    public Price multiplyBy(float value) {

        return (new Price(this.value * value));
    }

    /**
     * Multiplies the current price by another price
     * 
     * @param price to multiply price
     * @return the new price
     */
    public Price multiplyBy(Price price) {

        return (new Price(this.value * price.getValue()));
    }

    /**
     * Substracts the current price by another price
     * 
     * @param price to substract price
     * @return the new price
     */
    public Price substractBy(Price price) {

        return (new Price(this.value - price.getValue()));
    }

    /**
     * Compares to different prices to be sorted
     * 
     * @param other the price to be compared
     */
    /*@Override
    public int compareTo(Object other) {

        Price toComparePrice = (Price)other;
        float toComparePriceValue = toComparePrice.getValue();

        return (Float.compare(
            this.value, toComparePriceValue));
    }*/

}