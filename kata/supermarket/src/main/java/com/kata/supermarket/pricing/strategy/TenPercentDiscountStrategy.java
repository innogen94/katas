package com.kata.supermarket.pricing.strategy;

/**
 * Applies a 10 percent discount strategy for a quantified given item
 * 
 * @author Volodia WOJCIUK
 */
public class TenPercentDiscountStrategy extends PercentBasedDiscountStrategy {

    /**
     * Initializes the discount strategy
     * 
     * @param percentage
     */
    public TenPercentDiscountStrategy() {

        super("Ten percent discount", 10.0f);
    }    
}