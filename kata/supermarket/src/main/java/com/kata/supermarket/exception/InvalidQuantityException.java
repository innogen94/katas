package com.kata.supermarket.exception;

public class InvalidQuantityException extends RuntimeException {

    /**
     * Serial number
     */
    private static final long serialVersionUID = 1L;

    /**
     * Initializes the exception
     * 
     * @param message the exception message
     */
    public InvalidQuantityException(String message) {

        super(message);
    }

}