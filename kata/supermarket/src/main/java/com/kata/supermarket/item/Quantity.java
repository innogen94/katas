package com.kata.supermarket.item;

import com.kata.supermarket.exception.InvalidQuantityException;
import com.kata.supermarket.exception.NotEnoughItemQuantityException;

import lombok.Data;

@Data
public class Quantity {
    
    /**
     * Properties
     */
    private float value;

    /**
     * Constructs a new quantity
     * 
     * @param value
     */
    private Quantity(float value) {

        if (Float.compare(value, 0.0f) < 0) {
            throw new InvalidQuantityException(
                "A quantity has to be greater than or equal to zero.");
        }
        this.value = value;
    }

    /**
     * Create a new entity with a given value
     * 
     * @param value the quantity value
     * @return the new quantity
     */
    public static Quantity of(float value) {

        return (new Quantity(value));
    }

    /**
     * Increases the quantity by one unit
     */
    public Quantity increase() {

        this.increaseBy(1);
        return (this);
    }

    /**
     * Increases the quantity by a given value
     * 
     * @param value the value to add
     */
    public Quantity increaseBy(int value) {

        this.value += value;
        return (this);
    }

    /**
     * Multiplies the current quantity by another quantity
     * 
     * @param price to multiply quantity
     * @return the new quantity
     */
    public Quantity multiplyBy(Quantity quantity) {

        return (new Quantity(this.value * quantity.getValue()));
    }

    /**
     * Check if the current quantity is greater than the other one
     * 
     * @param quantity the quantity to compare
     * @return the boolean status
     */
    public boolean greaterThan(Quantity quantity) {

        return (this.value > quantity.getValue());
    }

    /**
     * Check if the current quantity is smaller than the other one
     * 
     * @param quantity the quantity to compare
     * @return the boolean status
     */
    public boolean smallerThan(Quantity quantity) {

        return (this.value < quantity.getValue());
    }

    /**
     * Substracts the current quantity by another quantity
     * 
     * @param quantity to substract quantity
     * @return the new price
     */
    public Quantity substractBy(Quantity quantity) {

        if (this.smallerThan(quantity)) {
            throw new NotEnoughItemQuantityException(
                "The maximum amount of item quantity has been exceeded.");
        }
        return (new Quantity(this.value - quantity.getValue()));
    }

    /**
     * Substracts the current quantity by another quantity
     * 
     * @param price to substract value
     * @return the new price
     */
    public Quantity substractBy(int value) {

        if (this.value < value) {
            throw new NotEnoughItemQuantityException(
                "The maximum amount of item quantity has been exceeded.");
        }
        return (new Quantity(this.value - value));
    }

    /**
     * Substracts the current quantity by another quantity
     * 
     * @param price to substract value
     * @return the new price
     */
    public Quantity dividedBy(int value) {

        if (this.value == 0) {
            throw new RuntimeException(
                "Cannot divide a quantity by zero");
        }
        return (new Quantity(this.value / value));
    }
}