package com.kata.supermarket.pricing.strategy;

import com.kata.supermarket.item.QuantifiedItem;
import com.kata.supermarket.item.Quantity;
import com.kata.supermarket.pricing.*;

/**
 * Describes Buy Two Get One Free pricing strategy
 * 
 * @author Volodia WOJCIUK
 */
public class BuyTwoGetOneFreeStrategy extends PricingStrategy {

    /**
     * Minimum quantity to discount
     */
    private final static int QUANTITY_TO_DISCOUNT = 3;

    public BuyTwoGetOneFreeStrategy() {

        super("Buy two get one free");
    }

    /**
     * Applies Buy Two Get One Free princing strategy
     * 
     * @param quantifiedItem the quantified item
     */
    @Override
    public Price applyTo(QuantifiedItem quantifiedItem) {

        Price itemPrice = quantifiedItem.getItem().getPrice();
        Quantity itemQuantity = quantifiedItem.getQuantity();
        Quantity discountQuantity = Quantity.of(itemQuantity.getValue() / QUANTITY_TO_DISCOUNT);

        Price offer = Price.of(itemPrice.getValue() * discountQuantity.getValue());
        Price baseTotalPrice = quantifiedItem.getTotalPrice();
        Price discountedPrice = Price.of(baseTotalPrice.getValue() - offer.getValue());

        return (discountedPrice);
    }
}