package com.kata.supermarket.pricing.strategy;

import com.kata.supermarket.item.QuantifiedItem;
import com.kata.supermarket.pricing.Price;

public class PercentBasedDiscountStrategy extends PricingStrategy {
    
    /**
     * Properties
     */
    private float percentage;
    
    /**
     * Initialization
     * 
     * @param percentage the percentage
     */
    public PercentBasedDiscountStrategy(String name, float percentage) {

        super(name);
        this.percentage = percentage;
    }

    /**
     * Applies the given pricing strategy
     * 
     * @param quantifiedItem the quantified item
     */
    @Override
    public Price applyTo(QuantifiedItem quantifiedItem) {

        Price baseTotalPrice = quantifiedItem.getTotalPrice();

        Price offer = baseTotalPrice.multiplyBy(this.percentage / 100.0f);
        Price discountedPrice = 
            baseTotalPrice.substractBy(offer);

        return (discountedPrice);
    }
}