package com.kata.supermarket.item;

import com.kata.supermarket.pricing.Price;
import com.kata.supermarket.pricing.strategy.PricingStrategy;

public interface Discountable {
    
    Price discountWith(PricingStrategy pricingStrategy);
}