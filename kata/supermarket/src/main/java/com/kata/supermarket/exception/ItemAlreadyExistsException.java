package com.kata.supermarket.exception;

/**
 * Describes an Item Already Exists Exception
 * 
 * @author Volodia WOJCIUK
 */
public class ItemAlreadyExistsException extends RuntimeException {
    
    /**
     * Serial number
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new Item Already Exists Exception
     * 
     * @param message the message of the exception
     */
    public ItemAlreadyExistsException(String message) {

        super(String.format("Item Already Exists Exception. Cause: %s\n", message));
    }
}