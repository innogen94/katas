package com.kata.supermarket.utils;

import java.util.Optional;

public class CommonUtil {
    
    /**
     * Check if a given instance is a null pointer
     * 
     * @param object
     * @return
     */
    public static boolean isNull(Object object) {

        return (!Optional.ofNullable(object).isPresent());
    }
}