package com.kata.supermarket.exception;

/**
 * Describes an Invalid Quantified Item Exception
 * 
 * @author Volodia WOJCIUK
 */
public class InvalidQuantifiedItemException extends RuntimeException {

    /**
     * Serial number
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new Invalid Item Exception
     * 
     * @param message the message of the exception
     */
    public InvalidQuantifiedItemException(String message) {

        super(message);
    }
}