package com.kata.supermarket.exception;

/**
 * Describes an Invalid Item Exception
 * 
 * @author Volodia WOJCIUK
 */
public class InvalidItemException extends RuntimeException {

    /**
     * Serial number
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new Invalid Item Exception
     * 
     * @param message the message of the exception
     */
    public InvalidItemException(String message) {

        super(message);
    }
}