package com.kata.supermarket.pricing;

import static org.junit.Assert.assertEquals;

import com.kata.supermarket.exception.InvalidPriceException;

import org.junit.Test;

public class PriceTest {
    
    /**
     * Given
     *  Negative price value
     * When
     *  Try to create a new price
     * Then
     *  Throws InvalidPriceException
     */
    @Test(expected = InvalidPriceException.class)
    public void shouldThrowExceptionWhenNegativePriceValue() {

        Price.of(-2.5f);
    }

    /**
     * Given
     *  Price value greater or equal to zero
     * When
     *  Trying to create a new price
     * Then
     *  Succeeded
     */
    @Test
    public void shouldSucceedWhenPriceValueIsGreaterOrEqualToZero() {

        Price zeroPrice = Price.of(0.0f);
        Price positivePrice = Price.of(2.99f);

        assertEquals(0, Float.compare(zeroPrice.getValue(), 0.0f));
        assertEquals(0, Float.compare(positivePrice.getValue(), 2.99f));
    }

    /**
     * Given
     *  Price value equal to zero
     * When
     *  Check if price equal to zero
     * Then
     *  True
     */
    @Test
    public void shouldReturnTrueWhenCheckZeroPriceValue() {

        Price zeroPrice = Price.of(0.0f);

        assertEquals(true, zeroPrice.isEqualToZero());
    }

    /**
     * Given
     *  Price value greater than zero
     * When
     *  Check if price is positive
     * Then
     *  True
     */
    @Test
    public void shouldReturnTrueWhenCheckPositivePriceValue() {

        Price positivePrice = Price.of(2.99f);

        assertEquals(true, positivePrice.isPositive());
    }

    /**
     * Given
     *  Price of 2€
     * When
     *  Multiply price by 3
     * Then
     *  Returns a price of 6€
     */
    @Test
    public void shouldReturnSixWhenMultiplyPriceOfTwoByValueEqualsToThree() {

        Price price = Price.of(2.0f);
        int quantity = 3;

        Price finalPrice = price.multiplyBy(quantity);

        assertEquals(Price.of(6.0f), finalPrice);
    }

    /**
     * Given
     *  3 different prices (2€, 4.9€ and 2.5€)
     * When
     *  Multiply all together
     * Then
     *  Returns a price of 24.5€
     */
    @Test
    public void shouldSucceedWhenMultiplySeveralPricesTogether() {

        Price price1 = Price.of(2.0f);
        Price price2 = Price.of(4.9f);
        Price price3 = Price.of(2.5f);

        Price finalPrice = price1
            .multiplyBy(price2)
            .multiplyBy(price3);

        assertEquals(Price.of(24.5f), finalPrice);
    }
}