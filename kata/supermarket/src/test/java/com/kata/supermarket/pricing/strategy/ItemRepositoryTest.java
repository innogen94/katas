package com.kata.supermarket.pricing.strategy;

import static org.junit.Assert.assertEquals;

import com.kata.supermarket.exception.ItemNotFoundException;
import com.kata.supermarket.item.Item;
import com.kata.supermarket.item.QuantifiedItem;
import com.kata.supermarket.item.Quantity;
import com.kata.supermarket.pricing.Price;
import com.kata.supermarket.repository.ItemRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemRepositoryTest {
    
    /**
     * Item repository
     */
    @Autowired
    private ItemRepository itemRepository;

    /**
     * Given
     *  Registered Item (A) by default
     * When
     *  Try to get item quantity (1) after registering check
     * Then
     *  Succeed
     */
    @Test
    public void shouldHaveItemQuantityOfOneWhenAddItemWithoutQuantity() {

        //Given

        Item itemA = Item.create("A", Price.of(3.2f));
        this.itemRepository.addItem(itemA);

        //When

        boolean isRegistered = this.itemRepository.isItemRegistered(itemA.getName());
        Quantity quantity = this.itemRepository.getQuantity(itemA.getName());
        
        //Then

        assertEquals(true, isRegistered);
        assertEquals(Quantity.of(1), quantity);
    }

    /**
     * Given
     *  Registered Item (A)
     * When
     *  Try to get item quantity (5) after registering check
     * Then
     *  Succeed
     */
    @Test
    public void shouldHaveItemQuantityOfFiveWhenAddItemWithQuantityOfFive() {

        //Given

        Item itemA = Item.create("A", Price.of(3.2f));
        this.itemRepository.addItem(itemA, Quantity.of(5));

        //When

        boolean isRegistered = this.itemRepository.isItemRegistered(itemA.getName());
        Quantity quantity = this.itemRepository.getQuantity(itemA.getName());

        //Then

        assertEquals(true, isRegistered);
        assertEquals(Quantity.of(5), quantity);
    }

    /**
     * Given
     *  Registered Item (A)
     * When
     *  Try to get item quantity after registering check
     * Then
     *  Succeed
     */
    @Test(expected = ItemNotFoundException.class)
    public void shouldThrowExceptionWhenTryToGetItemQuantityAfterRepositoryFlushed() {

        //Given

        Item itemA = Item.create("A", Price.of(3.2f));
        this.itemRepository.addItem(itemA, Quantity.of(5));

        //When

        this.itemRepository.flush();
        this.itemRepository.getQuantity(itemA.getName());
    }

    /**
     * Given
     *  RegistereR 5 Item of type A
     * When
     *  Try to get 3 items from repository
     * Then
     *  2 remaining item in repository
     */
    @Test
    public void shouldRemainingItemQuantityOfTwoWhenGetThreeItemsFromRepository() {

        //Given

        Item itemA = Item.create("A", Price.of(3.2f));
        Quantity itemQuantity = Quantity.of(5);
        this.itemRepository.addItem(itemA, itemQuantity);

        //When

        QuantifiedItem quantifiedItem = 
            this.itemRepository.getItems(itemA.getName(), Quantity.of(3)); 
        Quantity remainingQuantity = this.itemRepository.getQuantity(itemA.getName());

        //Then

        assertEquals(itemA, quantifiedItem.getItem());
        assertEquals(Quantity.of(3), quantifiedItem.getQuantity());
        assertEquals(Quantity.of(2), remainingQuantity);
    }
}