package com.kata.supermarket.pricing.strategy;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import com.kata.supermarket.item.Item;
import com.kata.supermarket.pricing.Price;
import com.kata.supermarket.repository.StrategyRepository;

import org.junit.Before;
import org.junit.Test;

public class StrategyRepositoryTest {

    /**
     * Properties
     */
    private StrategyRepository strategyRepository;

    /**
     * Initialization
     */
    @Before
    public void setUp() {

        this.strategyRepository = new StrategyRepository();
    }

    /**
     * Given
     *  Invalid strategy (null)
     * When
     *  Try associate pricing strategy (null) to a given item (A)
     * Then
     *  throw NullPointerException
     */
    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenNameIsNull() {

        Item itemA = Item.create("A", Price.of(1.75f));
        
        this.strategyRepository.addPricingStrategyToItem(null, itemA);
    }

    /**
     * Given
     *  Invalid item (null)
     * When
     *  Try associate pricing strategy 
     *  (BuyTwoGetOneFreeStrategy) to a given item (null)
     * Then
     *  throw NullPointerException
     */
    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenPriceIsNull() {

        PricingStrategy pricingStrategy = new BuyTwoGetOneFreeStrategy();

        this.strategyRepository.addPricingStrategyToItem(pricingStrategy, null);
    }
    
    /**
     * Given
     *  Valid pricing strategy and item
     * When
     *  Try associate pricing strategy 
     *  (BuyTwoGetOneFreeStrategy) to a given item (A)
     * Then
     *  Succeed
     */
    @Test
    public void givenItemAWhenAddStrategyToItemThenSucceed() {

        //Given

        Item itemA = Item.create("A", Price.of(1.75f));

        //When

        this.strategyRepository.addPricingStrategyToItem(new BuyTwoGetOneFreeStrategy(), itemA);
        Optional<PricingStrategy> pricingStrategy = 
                this.strategyRepository.getPricingStrategy(itemA);
        //Then

        assertEquals(true, pricingStrategy.isPresent());
    }
    
}