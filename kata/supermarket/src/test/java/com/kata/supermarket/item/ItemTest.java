package com.kata.supermarket.item;

import static org.junit.Assert.assertEquals;

import com.kata.supermarket.exception.InvalidItemException;
import com.kata.supermarket.pricing.Price;

import org.junit.Test;

public class ItemTest {

    /**
     * Given
     *  Null name
     * When
     *  Try to create a new item
     * Then
     *  throw InvalidItemException
     */
    @Test(expected = InvalidItemException.class)
    public void shouldThrowExceptionWhenNameIsNull() {

        Item.create(null, Price.of(2.5f));
    }

    /**
     * Given
     *  Empty name
     * When
     *  Try to create a new item
     * Then
     *  throw InvalidItemException
     */
    @Test(expected = InvalidItemException.class)
    public void shouldThrowExceptionWhenNameIsEmpty() {

        Item.create("", Price.of(2.5f));
    }

    /**
     * Given
     *  Null price
     * When
     *  Try to create a new item
     * Then
     *  throw InvalidItemException
     */
    @Test(expected = InvalidItemException.class)
    public void shouldThrowExceptionWhenPriceIsNull() {

        Item.create("A", null);
    }

    /**
     * Given
     *  Valid parameters
     * When
     *  Try to create a new item
     * Then
     *  Succeed
     */
    @Test
    public void shouldSucceedWhenNameAndPriceAreValid() {

        //Given

        String itemName = "A";
        Price itemPrice = Price.of(2.5f);

        //When

        Item item = Item.create(itemName, itemPrice);

        //Then

        assertEquals(true, item.getName().equals(itemName));
        assertEquals(true, item.getPrice().equals(itemPrice));
    }
}