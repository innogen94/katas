package com.kata.supermarket.item;

import static org.junit.Assert.assertEquals;

import com.kata.supermarket.exception.InvalidQuantifiedItemException;
import com.kata.supermarket.pricing.Price;

import org.junit.Test;

public class QuantifiedItemTest {
 
    /**
     * Given
     *  Invalid item (null)
     * When
     *  Try to create a new quantified item
     * Then
     *  throw InvalidQuantifiedItemException
     */
    @Test(expected = InvalidQuantifiedItemException.class)
    public void shouldThrowExceptionWhenItemIsNull() {

        QuantifiedItem.create(null, Quantity.of(3));
    }

    /**
     * Given
     *  Valid item
     * When
     *  Try to create a new quantified item
     * Then
     *  Succeed
     */
    @Test
    public void shouldSucceedWhenValidItem() {

        //Given

        Item item = Item.create("A", Price.of(10.5f));
        Quantity quantity = Quantity.of(3);

        //When
        
        QuantifiedItem quantifiedItem = 
            QuantifiedItem.create(item, quantity);

        //Then

        assertEquals(quantifiedItem.getItem(), item);
        assertEquals(quantifiedItem.getQuantity(), quantity);
    }

    
}