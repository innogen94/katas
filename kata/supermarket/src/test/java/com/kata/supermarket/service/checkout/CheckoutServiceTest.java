package com.kata.supermarket.service.checkout;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.kata.supermarket.item.Item;
import com.kata.supermarket.item.QuantifiedItem;
import com.kata.supermarket.item.Quantity;
import com.kata.supermarket.pricing.Price;
import com.kata.supermarket.pricing.strategy.BuyTwoGetOneFreeStrategy;
import com.kata.supermarket.pricing.strategy.TenPercentDiscountStrategy;
import com.kata.supermarket.repository.ItemRepository;
import com.kata.supermarket.repository.StrategyRepository;

import static org.assertj.core.api.Assertions.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CheckoutServiceTest {

    /**
     * Item repository
     */
    @Autowired
    private ItemRepository itemRepository;

    /**
     * Pricing strategy repositiory
     */
    @Autowired
    private StrategyRepository strategyRepository;

    /**
     * Checkout
     */
    @Autowired
    private CheckoutService checkout;

    /**
     * Initialization
     */
    @Before
    public void setUp() {

        Item A = Item.create("A", Price.of(2.0f));
        Item B = Item.create("B", Price.of(0.5f)); //No discount
        Item C = Item.create("C", Price.of(3.5f));

        this.itemRepository.addItem(A, Quantity.of(3));
        this.itemRepository.addItem(B, Quantity.of(8));
        this.itemRepository.addItem(C, Quantity.of(2));

        this.strategyRepository.addPricingStrategyToItem(new BuyTwoGetOneFreeStrategy(), A);
        this.strategyRepository.addPricingStrategyToItem(new TenPercentDiscountStrategy(), C);
    }

    /**
     * Given
     *  3 items (A) each costs 2€
     * When
     *  Scan items applying a 'Buy two get one free' strategy
     * Then
     *  Get discounted price of 4€ instead of 6€
     */
    @Test
    public void givenThreeItemsEachPriceEqualToTwoWhenScanningThenPriceEqualToFour() {

        //Given

        QuantifiedItem quantifiedItemA = this.itemRepository.getItems("A", Quantity.of(3));
        
        //When
        
        Price discountedPrice = this.checkout.scan(quantifiedItemA);

        //Then
    
        assertThat(discountedPrice).isEqualTo(Price.of(4.0f));
    }

    /**
     * Given
     *  2 items (C) each costs 3.5€
     * When
     *  Scanning items applying a 'Ten percent discount' strategy
     * Then
     *  Get discounted price of 6.3€ instead of 7€
     */
    @Test
    public void givenTwoItemsWhenScanningThenPriceShouldBeEqualToSixPointThree() {

        //Given

        QuantifiedItem quantifiedItemC = this.itemRepository.getItems("C", Quantity.of(2));

        //When

        Price discountedPrice = this.checkout.scan(quantifiedItemC);

        //Then

        assertThat(discountedPrice).isEqualTo(Price.of(6.3f));
    }

    /**
     * Given
     *  2 items (C) each costs 3.5€
     * When
     *  Scanning items without any discount rule
     * Then
     *  Get expected price of 7€
     */
    @Test
    public void givenEightItemsWhenScanningThenPriceShouldBeEqualToFour() {

        //Given

        QuantifiedItem quantifiedItemB = this.itemRepository.getItems("B", Quantity.of(8));

        //When

        Price discountedPrice = this.checkout.scan(quantifiedItemB);

        //Then

        assertThat(discountedPrice).isEqualTo(Price.of(4.0f));
    }

    /**
     * Given
     *  3 items (A) each costs 2€ and 7 items (B) each costs 0.5€
     * When
     *  Scanning items appling 'Buy-two-get-one-free' strategy for items (A)
     * Then
     *  Get total discounted price of 7.5€ instead of 9.5€
     */
    @Test
    public void shouldReturnsDiscountedPriceWhenApplyingPricingRule() {

        //Given

        QuantifiedItem qi1 = this.itemRepository.getItems("A", Quantity.of(3));
        QuantifiedItem qi2 = this.itemRepository.getItems("B", Quantity.of(7));

        List<QuantifiedItem> quantifiedItems = 
            Stream.of(qi1, qi2).collect(Collectors.toList());

        //When

        Price totalPrice = this.checkout.scan(quantifiedItems);

        //Then

        assertThat(totalPrice).isEqualTo(Price.of(7.5f));
    }
}