package com.kata.supermarket.item;

import static org.junit.Assert.assertEquals;

import com.kata.supermarket.exception.InvalidQuantityException;

import org.junit.Test;

public class QuantityTest {
    
    /**
     * Given
     *  Negative quantity value
     * When
     *  Try to create the quantity
     * Then
     *  Throw InvalidQuantityException
     */
    @Test(expected = InvalidQuantityException.class)
    public void shouldThrowExceptionWhenNegativeQuantityValue() {

        Quantity.of(-1.5f);
    }

    /**
     * Given
     *  Negative quantity value
     * When
     *  Try to create the quantity
     * Then
     *  Throw InvalidQuantityException
     */
    @Test
    public void shouldSucceedWhenValidQuantityValue() {

        float quantityValue = 1.5f;
        Quantity quantity = Quantity.of(quantityValue);

        assertEquals(true, Float.compare(
            quantityValue, quantity.getValue()) == 0);
    }

    /**
     * Given
     *  Quantity of 2
     * When
     *  Increase quantity by one
     * Then
     *  Returns new quantity of 3
     */
    @Test
    public void shouldReturnThreeWhenIncreaseQuantityEqualToTwoByOne() {

        Quantity quantity1 = Quantity.of(2);
        Quantity increasedQuantity1 = quantity1.increase();
        Quantity quantity2 = Quantity.of(2);
        Quantity increasedQuantity2 = quantity2.increaseBy(1);

        assertEquals(Quantity.of(3), increasedQuantity1);
        assertEquals(Quantity.of(3), increasedQuantity2);
    }
    
}