package com.kata.bank.exception;

public class InvalidAmountException extends RuntimeException {

    private static final long serialVersionUID = -5303171905229298994L;

    public InvalidAmountException(String message) {

        super(message);
    }
}