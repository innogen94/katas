package com.kata.bank.model;

import lombok.Data;

@Data
public class User {

    /**
     * Properties
     */
    private String firstname;
    private String lastname;
    
}