package com.kata.bank.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Operation {

    /**
     * Properties
     */
    private BankAccount     account;
    private OperationType   type;
    private Amount          amount;
    private String          description;

}