package com.kata.bank.model;

import java.util.List;

import com.kata.bank.exception.InvalidAmountException;

import lombok.Getter;
import lombok.Setter;

/**
 * Personal account
 * 
 * @author Volodia WOJCIUK
 * 
 */
public class BankAccount {

    /**
     * Properties
     */
    @Getter
    private User user;
    private Balance balance;
    @Getter @Setter
    private Amount overdraft;
    //private List<Operation> operations;

    /**
     * Initializes the account with a balance equals to zero
     */
    private BankAccount() {

        this.balance.reset();
    }

    /**
     * Initializes the account with an initial balance
     * 
     * @param balance the initial balance
     */
    private BankAccount(Balance balance) {

        this.balance = balance;
    }

    /**
     * Create a new personal account with no balance
     */
    public static BankAccount create() {

        return (new BankAccount());
    }

    /**
     * Create a new personal account with a default balance
     */
    public static BankAccount create(Balance balance) {

        return (new BankAccount(balance));
    }

    /**
     * Deposit a given amount of money
     * 
     * @param amount the amount
     */
    public void deposit(Amount amount) {

        this.balance.increaseOf(amount);
    }

    /**
     * Withdraw a given amount of money
     * 
     * @param amount the amount
     */
    public void withdraw(Amount amount) {

        if (!this.balance.isWithdrawable(amount)) {
            throw new InvalidAmountException(
                "The balance is not sufficient to withdraw " + amount + " €");
        }
        this.balance.decreaseOf(amount);
    }

    /**
     * @return the balance
     */
    public Balance getBalance() {

        return (this.balance);
    }

    /**
     * @return the account statement as String
     */
    public String getStatement() {

        return (String.format(
            "Balance: %d €", this.balance));
    }
    
}