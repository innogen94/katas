package com.kata.bank.model;

import com.kata.bank.exception.InvalidAmountException;

import lombok.Getter;

public final class Amount {
    
    /**
     * Properties
     */
    @Getter
    private final int value;

    /**
     * Initializes a new amount
     * 
     * @param value the value of the amount
     */
    private Amount(int value) {

        if (value <= 0) {
            throw new InvalidAmountException(
                "An amount has to be positive or equal to zero");
        }
        this.value = value;
    }

    /**
     * Creates a new amount
     * 
     * @param value the value of the amount
     * @return the new amount
     */
    public static Amount of(int value) {

        return (new Amount(value));
    }

    /**
     * Substracts current amount with another one
     * 
     * @param amount the amount to be substracted
     * @return the resulted amount
     */
    public Amount substractedBy(Amount amount) {

        return (new Amount(this.value - amount.getValue()));
    }

    /**
     * Adds current amount with another one
     * 
     * @param amount the amount to be added
     * @return the resulted amount
     */
    public Amount addedBy(Amount amount) {

        return (new Amount(this.value - amount.getValue()));
    }

    /**
     * Check if the current amount is greater than or equal to the other one
     * 
     * @param amount the amount to be compared
     * @return the compare status
     */
    public boolean isGreaterThanOrEqualTo(Amount amount) {

        return (this.value >= amount.getValue());
    }

    /**
     * Check if the current amount is greater than the other one
     * 
     * @param amount the amount to be compared
     * @return the compare status
     */
    public boolean isGreaterThan(Amount amount) {

        return (this.value > amount.getValue());
    }

}