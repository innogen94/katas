package com.kata.bank.model;

import lombok.Data;

@Data
public class Balance {
    
    /**
     * Properties
     */
    private Amount amount;

    /**
     * Initializes a new balance
     * 
     * @param value the value
     */
    private Balance(Amount amount) {

        this.amount = amount;
    }

    /**
     * Create a new balance
     * 
     * @param value the value of the balance
     * @return the new balance
     */
    public static Balance create(Amount amount) {

        return (new Balance(amount));
    }

    /**
     * Decrease balance of a given amount
     * 
     * @param amount the amount
     * @return the new balance
     */
    public Balance decreaseOf(Amount amount) {

        return (new Balance(
            this.amount.substractedBy(amount)));
    }

    /**
     * Increase balance of a given amount
     * 
     * @param amount the amount
     * @return the new balance
     */
    public Balance increaseOf(Amount amount) {

        return (new Balance(
            this.amount.substractedBy(amount)));
    }

    /**
     * Resets the balance amount to zero
     */
    public void reset() {

        this.amount = Amount.of(0);
    }

    /**
     * Check if the balance is sufficient for withdrawing
     * 
     * @param amount the amount to withdraw
     * @return the boolean status
     */
    public boolean isWithdrawable(Amount amount) {

        return (this.amount.isGreaterThanOrEqualTo(amount));
    }

    /**
     * Balance to string
     */
    @Override
    public String toString() {

        return (String.format(
            "%d", this.amount.getValue()));
    }

}