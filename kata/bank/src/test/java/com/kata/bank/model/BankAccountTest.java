package com.kata.bank.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.kata.bank.exception.InvalidAmountException;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class BankAccountTest {
    
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void create_WithNegativeAmount_ThrowException() {

        Balance balance = Balance.create(Amount.of(-42));

        assertThatThrownBy(
            () -> BankAccount.create(balance))
            .isInstanceOf(InvalidAmountException.class)
            .hasMessage(
                "The amount to be deposited " + 
                "has to be greater than or equal to zero");
    }
    
    /*@Test
    public void create_WithoutInitialBalance_Equals() {

        Account account = PersonalAccount.create();

        assertThat(account.getBalance()).isEqualTo(0);
    }

    @Test
    public void create_WithInitialBalance_Equals() {

        Balance balance = Balance.create(Amount.of(42));
        Account account = PersonalAccount.create(balance);

        assertThat(account.getBalance()).isEqualTo(balance);
    }

    @Test
    public void deposit_WithNegativeAmount_ThrowException() {

        Account account = PersonalAccount.create();

        //doThrow(toBeThrown)

        assertThatThrownBy(
            () -> account.deposit(-42))
            .isInstanceOf(InvalidAmountException.class)
            .hasMessage(
                "The amount to be deposited " + 
                "has to be greater than or equal to zero");
    }

    @Test
    public void deposit_WhenNonNegativeAmount_NewBalance() {

        Account account = PersonalAccount.create(10);

        account.deposit(32);

        assertThat(account.getBalance()).isEqualTo(42);
    }

    @Test
    public void withdraw_WithNegativeAmount_ThrowException() {

        Account account = PersonalAccount.create();

        assertThatThrownBy(
            () -> account.withdraw(-42))
            .isInstanceOf(InvalidAmountException.class)
            .hasMessage(
                "The amount to be withdrawn " + 
                "has to be greater than or equal to zero");
    }

    @Test
    public void withdraw_WithAmountGreaterThanBalance_ThrowException() {

        Account account = PersonalAccount.create(42);
        int toWithdrawAmount = 43;

        assertThatThrownBy(
            () -> account.withdraw(toWithdrawAmount))
            .isInstanceOf(InvalidAmountException.class)
            .hasMessage(
                "The balance is not sufficient to withdraw " + toWithdrawAmount + " €");
    }

    @Test
    public void withdraw_WithValidAmount_NewBalance() {

        Account account = PersonalAccount.create(42);
        int toWithdrawAmount = 12;

        account.withdraw(toWithdrawAmount);

        assertThat(account.getBalance()).isEqualTo(30);

    }*/
    
}